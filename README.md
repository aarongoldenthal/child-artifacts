# Child Artifacts

Relates to: https://gitlab.com/groups/gitlab-org/-/epics/8205

## Getting started

Until GitLab provides a native way to pull in artifacts into the parent pipeline, this
is a workaround.

Two Key Constraints

1. Every child job's name must be unique.
1. All artifact file names must be unique.

